mmcore.IntegrationFactory.register('Ensighten', {
    defaults: {
        isStopOnDocEnd: false
    },
    validate: function (data) {
        if(!data.campaign)
          return 'No campaign.';
        if(!data.slot)
          return 'No custom variable slot.';
        if(data.slot < 1 || data.slot > 100 || isNaN(data.slot))
          return 'Invalid custom variable slot. Must be 1-100.';
        return true;
    },
    check: function (data) {
        return window.dataLayer || [];
    },
    exec: function (data)  {
        var prodSand = data.isProduction ? 'MM_Prod_' : 'MM_Sand_';
        window.dataLayer = window.dataLayer || []; 
        window.dataLayer.push({ 
        "maxymiserTests": [ 
        { 
        "Maxymiser Data": prodSand + data.campaignInfo, 
        "dimensionIndex": data.slot

        }
        ] 
        });
        
        if (typeof data.callback === 'function') data.callback();
        return true;
    }
});