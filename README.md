# Ensighten - Google Analytics

---

[TOC]

## Overview
This integration is used for extracting campaign experience data from the Maxymiser platform and sending it to a data layer in Ensighten.

###How we send the data
Our Ensighten Integration populates a custom dimension with the relevant campaign experience information. Each campaign creates a new object. 



###Data Format 
The data sent to Ensighten will be in the following format:

`<mode>_<campaign name>=<element1>:<variant>|<element2>:<variant>`

Please find additional information on the format of the sent data [here](https://bitbucket.org/mm-global-se/integration-factory#markdown-header-introduction
) 


##Prerequisite
The following information needs to be provided by the client: 

+ Campaign Name

+ Custom Variable/Slot 


## Download

* [ga-register.js](https://bitbucket.org/mm-global-se/if-ga-ensighten/src/master/src/ga-in-register.js)

* [ga-initialize.js](https://bitbucket.org/mm-global-se/if-ga-ensighten/src/master/src/ga-in-initialize.js)


## Deployment instructions

### Content Campaign

+ Ensure that you have the Integration Factory plugin deployed on site level([find out more](https://bitbucket.org/mm-global-se/integration-factory)). Map this script to the whole site with an _output order: -10_ 

+ Create another site script and add the [ga-en-register.js](https://bitbucket.org/mm-global-se/if-ga-ensighten/src/master/src/ga-en-register.js) script.  Map this script to the whole site with an _output order: -5_

    __NOTE!__ Please check whether integration already exists on site level. Do not duplicate the Integration Factory plugin and the Google Analytics Ensighten Register scripts!

+ Create a campaign script and add the [ga-en-initialize.js](https://bitbucket.org/mm-global-se/if-ga-ensighten/src/master/src/ga-en-initialize.js) script. Customise the code by changing the campaign name and slot number accordingly and add to campaign. Map this script to the page where the variants are generated on, with an _output order: -5_

### Redirect Campaign

For redirect campaigns, the campaign script needs to be mapped to both the generation and redirected page. To achieve this follow the steps bellow:

+ Go through the instructions outlined in [Content Campaign](https://bitbucket.org/mm-global-se/if-ga#markdown-header-content-campaign)

+ Make sure that in the initialize campaign script redirect is set to true.

+ Create and map an additional page where the redirect variant would land on. 

+ Map the integration initialize campaign script to this page as well.

## QA

* to QA this integration you will need to check the data layer in the console of the browser (note each campaign creates a new object).
* In the console type `window.dataLayer`. This should return several objects that will hold the Maxymiser Data:

![ensighten.PNG](https://bitbucket.org/repo/G4b855/images/1832153044-ensighten.PNG)

-